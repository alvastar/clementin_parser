<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends CI_Controller {

    public function async_getProductFromSession(string $sessionFilePath) : void {
        $this->load->model('Xml', 'xml');
        $sessionFilePath = FCPATH . 'storage' . DIRECTORY_SEPARATOR . 'sessions' . DIRECTORY_SEPARATOR . $sessionFilePath;
        $session = $this->xml->read($sessionFilePath);

        $hasFree = false;
        foreach ($session as $element) {
            if ($element['parser_status'] == -1) {
                $hasFree = true;
                echo json_encode(array(
                    'hasFree'   => $hasFree,
                    'uniqid'    => $element['uniqid'],
                    'productId' => $element['product_id']
                ));
                exit;
            }
        }

        echo json_encode(array(
            'hasFree' => $hasFree
        ));
        die();
    }

    public function async_getProductData(int $productId) : void {
        $this->load->model('Product', 'product');
        $productData = $this->product->getProductData($productId);
        if (preg_replace('~\s+[а-яА-Я]~s', null, $productData['link'])) {
            echo json_encode(array(
                'hasLink' => true,
                'sku'     => $productData['sku'],
                'name'    => $productData['name'],
                'link'    => $productData['link'] 
            ));
        } else {
            echo json_encode(array(
                'hasLink' => false
            ));
        }
        die();
    }

    public function async_getSite() : void {
    	if (isset($_POST['link'])) {
    		$link = strip_tags($_POST['link']);

    		$this->load->model('Xml', 'xml');
	    	$sitesFilePath = FCPATH . 'storage' . DIRECTORY_SEPARATOR . 'sites.xml';
	    	$sites = $this->xml->read($sitesFilePath);

	    	foreach ($sites as $site) {
	    		if (stristr($link, $site['url'])) {
	    			list($min, $max) = explode(',', $site['latency']);
	    			$site['latency'] = random_int($min, $max);
	    			echo json_encode($site);
	    			exit;
	    		} 
	    	}

	    	echo json_encode(array(
	    		'url' => 'none'
	    	));
    	}
        die();
    }

    public function async_parse() : void {
        if (isset($_POST['prodictId'], $_POST['uniqId'], $_POST['productLink'], 
                  $_POST['delta'],  $_POST['storage'], $_POST['session'], $_POST['site'])) {
            
            // Фильтруем данные
            $prodictId   = (int) strip_tags($_POST['prodictId']);
            $uniqId      = strip_tags($_POST['uniqId']);
            $productLink = strip_tags($_POST['productLink']);
            $delta       = (int) strip_tags($_POST['delta']);
            $storage     = strip_tags($_POST['storage']);
            $site        = strip_tags($_POST['site']);
            $session     = strip_tags($_POST['session']);

            // Загружаем необходимы модели
            $this->load->model('Xml', 'xml');
            $this->load->model('Product', 'product');
            $this->load->model('Grabber', 'grabber');
            require_once APPPATH . 'third_party' . DIRECTORY_SEPARATOR . 'simple_html_dom.php';

            // Загружаем данные
            $product    = $this->product->getProductData($prodictId);
            $reflink    = sprintf('https://www.google.com.ua/search?q=%s&oq%s&sourceid=chrome&ie=UTF-8', $product['name'], $product['name']);
            $rules      = $this->xml->read(FCPATH . 'storage' . DIRECTORY_SEPARATOR . 'rules' . DIRECTORY_SEPARATOR . $storage . '.xml');
            $sessionFilePath = FCPATH . 'storage' . DIRECTORY_SEPARATOR . 'sessions' . DIRECTORY_SEPARATOR . $session;
            $session    = $this->xml->read($sessionFilePath);

            // Получаем и сохраняем страницу
            $htmlString = $this->grabber->getHtmlString($productLink, $reflink);
            $dirname = FCPATH . 'storage' . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR . $storage;
            $filepath = $dirname . DIRECTORY_SEPARATOR . url_title(str_replace(array($site, '.html'), null, $productLink)) . '.html';
            if (!is_dir($dirname)) {
                mkdir($dirname, 0777, false);
            }
            file_put_contents($filepath, $htmlString);

            $htmldom = new simple_html_dom();
            $htmldom->load($htmlString);

            $i;
            foreach ($session as $index => $item) {
                if ($item['uniqid'] == $uniqId && $item['product_id'] == $prodictId) {
                    $i = $index;
                    break;
                }
            }

            foreach ($rules as $rule) {
                $selector = $rule['selector'];
                $event    = $rule['event'];
                $status   = $rule['status'];
                $datetime = $rule['datetime'];
                $price    = $rule['price'];

                $e = $htmldom->find($selector, 0);
                if ($e !== null) {
                    $session[$i]['parser_status'] = 1;
                    $session[$i]['event']         = $event;

                    if ($status == 0) $status = $product['status'];
                    $session[$i]['product_status'] = $status;

                    if ($datetime == 'true') {
                        $session[$i]['datetime'] = date('d.m.Y');
                    } else {
                        $session[$i]['datetime'] = $product['parsertime'];
                    }

                    if ($price == 'true') {
                        $session[$i]['old_price'] = $product['price'];
                        $session[$i]['new_price'] = $e->plaintext;
                    } else {
                        $session[$i]['old_price'] = $product['price'];
                        $session[$i]['new_price'] = $product['price'];
                    }
                    break;
                } else {
                     $session[$i]['parser_status'] = 0;
                }
            }

            file_put_contents($sessionFilePath, null);
            $this->xml->write($sessionFilePath, 'session', $session, false);
            echo json_encode($event);
            die();
        }
    }
}