<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parser extends CI_Controller {

    public function index() {
        redirect('parser/first-step');
    }

    public function productMap() {
        $this->load->model('Html', 'html');
        $this->load->view('parts/header', array(
            'title' => 'Шаг 1 - Составление карты товаров'
        ));

        $this->load->model('Category', 'category');
        $this->load->view('parser/category_picker', array(
            'categoryTree' => $this->category->treeToHtml(
                $this->category->buildTree()
            )
        ));

        $this->load->view('parts/footer');
    }

    public function prepareSession() {
        $this->load->model('Html', 'html');
        $this->load->view('parts/header', array(
            'title' => 'Шаг 2 - Подготовка сессии'
        ));

        if (isset($_POST['category_collection'], $_POST['status_collection'])) {
            $categoryCollection = json_decode($_POST['category_collection']);
            $statusCollection   = json_decode($_POST['status_collection']);

            $this->load->model('Product', 'product');

            $assignedProductCollection = $this->product->getAssignedProductCollection($categoryCollection);
            $assignedProductCollection = $this->product->filterProductCollectionByStatus($assignedProductCollection, $statusCollection);
            
            $sessionData = array();
            foreach ($assignedProductCollection as $productId) {
                $sessionData[] = array(
                    'product_id'    => $productId,
                    'parser_status' => -1
                );
            }

            $this->load->model('Xml', 'xml');
            $sessionFilePath = FCPATH . 'storage' . DIRECTORY_SEPARATOR . 'sessions' . DIRECTORY_SEPARATOR . 'session_' . date('d-m-Y-H-i') . '.xml';
            $this->xml->write(
                $sessionFilePath,
                'session',
                $sessionData
            );

            $this->load->view('parser/prepare_session', array(
                'sessionFilePath'        => $sessionFilePath,
                'productCollectionCount' => count($assignedProductCollection)
            ));
        }

        
        $this->load->view('parts/footer');
    }

    public function start(string $sessionFilePath) {
    	$this->load->model('Html', 'html');
    	$this->load->view('parts/header', array(
            'title' => 'Шаг 3 - Старт'
        ));

    	$this->load->model('Xml', 'xml');
        $sessionFilePath = FCPATH . 'storage' . DIRECTORY_SEPARATOR . 'sessions' . DIRECTORY_SEPARATOR . $sessionFilePath;
        $sessionData = $this->xml->read($sessionFilePath);

        $sessionDone = 0;
        foreach ($sessionData as $element) {
            if ($element['parser_status'] > -1) {
                $sessionDone++;
            }
        }

        $this->load->view('parser/session_start', array(
            'sessionFilePath' => $sessionFilePath,
            'sessionData'     => $sessionData,
            'sessionDone'     => $sessionDone
        ));

        $this->load->view('parts/footer');
    }

}