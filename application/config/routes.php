<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']   = 'Parser/index';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;

$route['parser/first-step']                = 'Parser/productMap';
$route['parser/second-step']               = 'Parser/prepareSession';
$route['parser/third-step/session/(:any)'] = 'Parser/start/$1';

$route['parser/process/getSessionElement/(:any)'] = 'Process/async_getProductFromSession/$1';
$route['parser/process/checkProduct/(:num)']      = 'Process/async_getProductData/$1';
$route['parser/process/getSite']                  = 'Process/async_getSite';
$route['parser/process/parse']                    = 'Process/async_parse';