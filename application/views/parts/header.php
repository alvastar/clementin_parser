<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex,nofollow">
  <title><?php echo $title; ?></title>
  <?php echo $this->html->css(); ?>
</head>
<body>
<div class="container mt-5 mb-5">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-block">
        <h1 class="text-primary text-center">
          <?php echo $title; ?>
        </h1>
        <hr>