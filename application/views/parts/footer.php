        </div><!-- .card-block -->
        <div class="card-footer text-muted text-center">
          <ul class="list-inline mb-0">
            <li class="list-inline-item">Версия: 1.1.0</li>
            <li class="list-inline-item">Время выполнения: {elapsed_time} сек.</li>
          </ul>
        </div>
      </div><!-- .card -->
    </div><!-- .col -->
  </div><!-- .row -->
</div><!-- .container -->
<?php echo $this->html->js(); ?>
</body>
</html>