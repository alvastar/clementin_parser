<section id="parser--step-1" class="class mb-3">
  <div id="category-picker">
    <h2>Дерево категорий</h2>
    <?php echo $categoryTree; ?>
  </div>
  <hr>
  <div id="product-status-picker">
    <h2>Статусы товаров</h2>
    <select id="product-statuses" class="form-control mb-3" size="5" multiple>
      <option value="1">Включен</option>
      <option value="2">Отключен</option>
      <option value="3">Доставка 2-4 дня</option>
      <option value="4">Нет в наличии</option>
      <option value="5">Архив</option>
    </select>
  </div>
  <hr>
  <form action="<?php echo base_url('index.php/parser/second-step'); ?>" method="POST">
  	<input type="hidden" id="data--category-collection" name="category_collection">
  	<input type="hidden" id="data--status-collection" name="status_collection">
  	<button role="button" type="submit" class="btn btn-primary">
      Подготовить сессию
    </button>
  </form>
</section>