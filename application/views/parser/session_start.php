<section id="parser--step-3">
  <p class="text-center text-muted">
    Для старта сессии всё готово :)
  </p>
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th style="width: 15%;">Файл сессии:</th>
        <td id="session"><?php echo basename($sessionFilePath); ?></td>
      </tr>
      <tr>
        <th style="width: 15%;">Кол-во товаров:</th>
        <td id="count"><?php echo count($sessionData); ?></td>
      </tr>
      <tr>
        <th style="width: 15%;">Выполнено:</th>
        <td id="done"><?php echo $sessionDone; ?></td>
      </tr>
    </tbody>
  </table>
  <hr>
  <a href="<?php echo base_url(); ?>" class="btn btn-secondary">
    К выбору категорий
  </a>
  <button role="button" id="session-start" class="btn btn-primary">
    Старт
  </button>
</section>
<hr>
<input type="hidden" id="base-url" value="<?php echo base_url('index.php/'); ?>">
<section id="parser--response" style="display: none;">
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th style="width: 15%;">ID элемента сессии:</th>
        <td id="uniqid"></td>
      </tr>
      <tr>
        <th style="width: 15%;">Товар:</th>
        <td id="product">
          <div id="product-id">Идентификатор:
            <span></span>
          </div>
          <div id="name">Наименование:
            <span></span>
          </div>
          <div id="sku">Артикул:
            <span></span>
          </div>
        </td>
      </tr>
      <tr>
        <th style="width: 15%;">Ссылка:</th>
        <td id="link"></td>
      </tr>
      <tr>
        <th style="width: 15%;">Сайт:</th>
        <td id="site"></td>
      </tr>
      <tr>
        <th style="width: 15%;">Ожидание:</th>
        <td id="latency"></td>
      </tr>
      <tr>
        <th style="width: 15%;">Статус парсера:</th>
        <td id="status"></td>
      </tr>
    </tbody>
  </table>
</section>