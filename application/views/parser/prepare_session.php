<section id="parser--step-2">
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th style="width: 15%;">Кол-во товаров:</th>
        <td><?php echo $productCollectionCount; ?></td>
      </tr>
      <tr>
        <th style="width: 15%;">Файл сессии:</th>
        <td><?php echo $sessionFilePath; ?></td>
      </tr>
    </tbody>
  </table>
  <hr>
  <a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="btn btn-secondary">
    Назад
  </a>
  <a href="<?php echo base_url('index.php/parser/third-step/session/' . basename($sessionFilePath)); ?>" class="btn btn-primary">
    Далее
  </a>
</section>