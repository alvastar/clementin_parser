<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Xml extends CI_Model {

    private $_xmlString = '';

    public function set(string $xmlFilePath) {
        if (file_exists($xmlFilePath)) {
            $this->_xmlString = file_get_contents($xmlFilePath);
        } else {
            die ('Xml-файла по адресу ' . $xmlFilePath . ' не существует!');
        }
    }

    public function read(string $xmlFilePath) : array {
        $this->set($xmlFilePath);

        $_READER = new XMLReader();
        $_READER->XML($this->_xmlString, 'UTF-8');

        $output = array();
        $index  = 0;

        while ($_READER->read()) {
            if ($_READER->depth == 1 && $_READER->nodeType == XMLReader::ELEMENT) {
                $tagName = $_READER->name;
            }

            if ($_READER->depth == 2 && $_READER->nodeType == XMLReader::ELEMENT) {
                $innerTagName = $_READER->name;
            }

            if ($_READER->depth == 3 && $_READER->nodeType == XMLReader::TEXT) {
                $output[$index][$innerTagName] = $_READER->value;
            }

            if ($_READER->depth == 1 && $_READER->nodeType == XMLReader::END_ELEMENT) {
                if ($_READER->name === $tagName) $index++;
            }
        }

        $_READER->close();

        return $output;
    }

    public function write(string $filePath, string $mainTagName, array $data, bool $uniqid = true) {
        $_WRITER = new XmlWriter();
        $_WRITER->openMemory();
        $_WRITER->setIndent(true);
        $_WRITER->setIndentString("\t");

        $_WRITER->startDocument('1.0', 'UTF-8');
        $_WRITER->startElement($mainTagName);
        $index = 0;
        foreach ($data as $element) {
            $_WRITER->startElement('element');
            if ($uniqid == true) {
                $_WRITER->writeElement('uniqid', uniqid());
            }
            
            foreach ($element as $tagName => $value) {
                $_WRITER->writeElement($tagName, $value);
            }
            $_WRITER->endElement();

            if (++$index % 100) {
                file_put_contents($filePath, $_WRITER->flush(true), FILE_APPEND);
            }
        }
        $_WRITER->endElement();

        file_put_contents($filePath, $_WRITER->flush(true), FILE_APPEND);
    }

}