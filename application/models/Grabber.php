<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Grabber extends CI_Model {

    private $_useragent = array(
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/48.0.2564.82 Chrome/48.0.2564.82 Safari/537.36',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0',
        'Mozilla/5.0 (Linux; Ubuntu 14.04) AppleWebKit/537.36 Chromium/35.0.1870.2 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'
    );

    public function __construct() {
        parent::__construct();
    }

    public function getHtmlString(string $link, string $referrer) {
        $curl = curl_init();                
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_REFERER, $referrer);
        curl_setopt($curl, CURLOPT_USERAGENT, $this->_useragent[mt_rand(0, count($this->_useragent) - 1)]);
        curl_setopt($curl, CURLOPT_HTTPGET, 1);
        $data = curl_exec($curl);    
        curl_close($curl);
        return $data;
    }
}