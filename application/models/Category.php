<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Model {

    private const EAV_ATTRIBUTE_ID_NAME = 41;

    private $_categoryCollection = array();
    private $_categoryTree       = array();
    private $_htmlTree           = '';

    private function _loadCollection() : void {
        $query = $this->db
            ->select(array(
                'catalog_category_entity.entity_id as id',
                'catalog_category_entity.parent_id as parentId',
                'catalog_category_entity_varchar.value as name'
            ))
            ->from('catalog_category_entity')
            ->join('catalog_category_entity_varchar', 
                'catalog_category_entity_varchar.entity_id = catalog_category_entity.entity_id ' .
                'AND catalog_category_entity_varchar.store_id = 0 ' .
                'AND catalog_category_entity_varchar.attribute_id = ' . self::EAV_ATTRIBUTE_ID_NAME
            )
            ->order_by('catalog_category_entity.entity_id')
            ->get();
        $this->_categoryCollection = $query->result_array();
    }

    public function buildTree() : array {
        $this->_loadCollection();
        foreach ($this->_categoryCollection as $_category) {
            $this->_categoryTree[$_category['parentId']][] = $_category;
        }
        return $this->_categoryTree;
    }

    public function treeToHtml($branch, int $parentId = 0, int $level = 0) : string {
        if (is_array($branch) && isset($branch[$parentId])) {
            $this->_htmlTree .= sprintf('<ol data-level="%d">', $level);
            
            foreach ($branch[$parentId] as $category) {
                $this->_htmlTree .= '<li>';
                $this->_htmlTree .= sprintf(
                    '<div class="form-check">
                      <label for="%s" class="form-check-label">
                        <input type="checkbox" id="category-%d" class="form-check-input" data-category="%d">
                        <span>%s</span>
                      </label>
                      <a href="#" role="button">
                        <i class="fa fa-fw fa-caret-down"></i>
                      </a>
                    </div>',
                    'category-' . $category['id'],  // Для label[for]
                    $category['id'],                // Для input[id]
                    $category['id'],                // Для input[data-category]
                    $category['name']               // Для span
                );

                $this->treeToHtml($branch, $category['id'], ++$level); $level--;
                $this->_htmlTree .= '</li>';
            }

            $this->_htmlTree .= '</ol>';
        }
        return $this->_htmlTree;
    }

}