<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Model {

    const EAV_ATTRIBUTE_ID_STATUS      = 96;
    const EAV_ATTRIBUTE_ID_NAME        = 71;
    const EAV_ATTRIBUTE_ID_PRICE       = 75;
    const EAV_ATTRIBUTE_ID_LINK        = 407;
    const EAV_ATTRIBUTE_ID_PARSER_TIME = 1029;

    public function getAssignedProductCollection(array $categoryCollection) : array {
        $query = $this->db 
            ->select('product_id')
            ->distinct()
            ->from('catalog_category_product')
            ->where_in('category_id', $categoryCollection)
            ->get();
        $assignedProductCollection = array();
        foreach ($query->result_array() as $product) {
            $assignedProductCollection[] = $product['product_id'];
        }
        return $assignedProductCollection;
    }

    public function filterProductCollectionByStatus(array $productCollection, array $statucCollection) : array {
        $filteredCollection = array();

        foreach ($productCollection as $productId) {
            $query = $this->db
                ->select('value')
                ->from('catalog_product_entity_int')
                ->where(array(
                    'attribute_id' => self::EAV_ATTRIBUTE_ID_STATUS,
                    'entity_id'    => $productId
                ))
                ->get();
            $productStatus = $query->row_array()['value'];

            if (in_array($productStatus, $statucCollection)) {
                $filteredCollection[] = $productId;
            }
        }

        return $filteredCollection;
    }

    public function getProductData(int $productId) : array {
        $query = $this->db
        ->select(array(
            'catalog_product_entity.entity_id as id',
            'catalog_product_entity.sku as sku',
            'catalog_product_entity_int.value as status',
            'catalog_product_entity_varchar.value as name',
            'catalog_product_entity_decimal.value as price',
            'catalog_product_entity_text.value as link',
            'catalog_product_entity_datetime.value as parsertime'
        ))
        ->from('catalog_product_entity')
        ->join('catalog_product_entity_int',
            'catalog_product_entity_int.entity_id = catalog_product_entity.entity_id ' . 
            'AND catalog_product_entity_int.store_id = 0 ' . 
            'AND catalog_product_entity_int.attribute_id = ' . self::EAV_ATTRIBUTE_ID_STATUS
        )
        ->join('catalog_product_entity_varchar',
            'catalog_product_entity_varchar.entity_id = catalog_product_entity.entity_id ' . 
            'AND catalog_product_entity_varchar.store_id = 0 ' .
            'AND catalog_product_entity_varchar.attribute_id = ' . self::EAV_ATTRIBUTE_ID_NAME
        )
        ->join('catalog_product_entity_decimal',
            'catalog_product_entity_decimal.entity_id = catalog_product_entity.entity_id ' .
            'AND catalog_product_entity_decimal.store_id = 0 ' .
            'AND catalog_product_entity_decimal.attribute_id = ' . self::EAV_ATTRIBUTE_ID_PRICE
        )
        ->join('catalog_product_entity_text',
            'catalog_product_entity_text.entity_id = catalog_product_entity.entity_id ' .
            'AND catalog_product_entity_text.store_id = 0 ' .
            'AND catalog_product_entity_text.attribute_id = ' . self::EAV_ATTRIBUTE_ID_LINK
        )
        ->join('catalog_product_entity_datetime',
            'catalog_product_entity_datetime.entity_id = catalog_product_entity.entity_id ' .
            'AND catalog_product_entity_datetime.store_id = 0 ' .
            'AND catalog_product_entity_datetime.attribute_id = ' . self::EAV_ATTRIBUTE_ID_PARSER_TIME
        )
        ->where('catalog_product_entity.entity_id', $productId)
        ->get();
        return $query->row_array();
    }
} 