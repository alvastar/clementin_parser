<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Html extends CI_Model {

    private $_cssToLoad = array(
        'bootstrap',
        'font-awesome.min'
    );

    private $_jsToLoad = array(
        'jquery-3.2.1.min',
        'tether.min',
        'bootstrap.min',
        'category-picker',
        'parser-proccess'
    );

    private function _getCssStorage() : string {
        return (FCPATH . 'assets/css/');
    }

    private function _getJsStorage() : string {
        return (FCPATH . 'assets/js/');
    }

    private function _getCssUrl() : string {
        return (base_url() . 'assets/css/');
    }

    private function _getJsUrl() : string {
        return (base_url() . 'assets/js/');
    }

    public function css() : string {
        $html = '';
        foreach ($this->_cssToLoad as $filename) {
            if (file_exists($this->_getCssStorage() . $filename . '.css')) {
                $html .= sprintf('<link rel="stylesheet" href="%s">', $this->_getCssUrl() . $filename . '.css');
            }
        }
        return $html;
    }

    public function js() : string {
        $html = '';
        foreach ($this->_jsToLoad as $filename) {
            if (file_exists($this->_getJsStorage() . $filename . '.js')) {
                $html .= sprintf('<script src="%s"></script>', $this->_getJsUrl() . $filename . '.js');
            }
        }
        return $html;
    }

}