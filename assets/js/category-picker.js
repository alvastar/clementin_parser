$(document).ready(function() {
    
    /* 
        Скрываем категории выше 3-го уровня 
    */
    var __categoryPickerContainer = $('#category-picker');
    $(__categoryPickerContainer).find('ol').each(function() {
        if ($(this).data('level') > 2) $(this).hide();
    });


    /*
        Скрываем кнопки разворачивания, где нет родительский категорий        
    */
    $(__categoryPickerContainer).find('.form-check').each(function() {
        if ($(this).next().length == 0) {
            $(this).find('a').hide();
        }
    });


    /*
        Разворачивание категорий
    */
    var __categoryExpandButton = $('#category-picker label + a');
    $(__categoryExpandButton).click(function(event) {
        event.preventDefault();
        $(this)
            .find('i')
            .toggleClass('fa-rotate-180');
        var __checkboxContainer = $(this).parent().parent();
        $(__checkboxContainer)
            .find('ol:first')
            .toggle(100);
    });

    
    /*
        Выбор категорий
    */
    var __categoryPickerCheckbox = $('#category-picker input[type="checkbox"]');
    $(__categoryPickerCheckbox).click(function() {
        var __checkboxContainer = $(this).parent().parent().parent();
        if ($(this).prop('checked')) {
            $(__checkboxContainer)
                .find('input[type="checkbox"]')
                .prop('checked', true);
        } else {
            $(__checkboxContainer)
                .find('input[type="checkbox"]')
                .prop('checked', false);
        }
    });


    /*
        Отправка данных
    */
    var __pushButton = $('#parser--step-1 button');
    $(__pushButton).click(function() {

        // Получаем список выбранных категорий
        var categoryCollection = [];
        $(__categoryPickerContainer).find('input[type="checkbox"]:checked').each(function() {
            categoryCollection.push($(this).data('category'));
        });
        if (categoryCollection.length == 0) {
            alert('Выберите хотя бы одну категорию!');
            return false;
        } else {
            $('#data--category-collection').val(
                JSON.stringify(categoryCollection)
            );
        }

        // Получаем список статусов товаров
        var productStatusCollection = $('#product-status-picker select').val();
        if (productStatusCollection.length == 0) {
            alert('Выберите хотя бы один статус товара!');
            return false;
        } else {
            $('#data--status-collection').val(
                JSON.stringify(productStatusCollection)
            );
        }

    });

});