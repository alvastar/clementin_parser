function ajax(url, data = null) {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: 'POST',
            url:  url,
            data : data,
            success: function(ServerResponse) {
                resolve(JSON.parse(ServerResponse));
            },
            error: function(ServerResponse) {
                reject(new Error('Ошибка!'));
            }
        });
    });
}



$(document).ready(function() {

    let baseUrl             = $('#base-url').val();
    let sessionFilePath     = $('#parser--step-3 #session').html();
    let sessionProductCount = $('#parser--step-3 #count').html();
    let done                = $('#parser--step-3 #done').html();

    let __latencyContainer      = $('#parser--response #latency');
    let __productContainer      = $('#parser--response #product');
    let __siteToParseContainer  = $('#parser--response #site');
    let __parserStatusContainer = $('#parser--response #status');
    let __linkToParseContainer  = $('#parser--response #link');
    let __sessionElementUniqIDContainer = $('#parser--response #uniqid');


    $('#parser--step-3 #session-start').click(function(event) {
        event.preventDefault();

        $('#parser--response').show(150);

        (function loop(i) {
            if (i <= sessionProductCount) new Promise(resolve => {
                ajax(baseUrl + 'parser/process/getSessionElement/' + sessionFilePath)
                    .then(session => {
                        let hasFree   = session.hasFree;
                        let uniqid    = session.uniqid;
                        let productId = session.productId;
                        if (hasFree == true) {
                            $(__sessionElementUniqIDContainer).html(uniqid);
                            $(__productContainer)
                                .find('#product-id span')
                                .html(productId);
                            return ajax(baseUrl + 'parser/process/checkProduct/' + productId);
                        }
                    })
                    .then(product => {
                        let hasLink     = product.hasLink;
                        let productLink = product.link;
                        let productName = product.name;
                        let productSku  = product.sku;
                        let uniqid = $(__sessionElementUniqIDContainer).html();
                        if (hasLink == true) {
                            $(__productContainer)
                                .find('#sku span')
                                .html(productSku);
                            $(__productContainer)
                                .find('#name span')
                                .html(productName);
                            $(__linkToParseContainer).html(productLink);
                            return ajax(baseUrl + 'parser/process/getSite', {link: productLink});
                        }
                    })
                    .then(site => {
                        if (site.url != 'none') {
                            let siteName    = site.name;
                            let siteUrl     = site.url;
                            let siteStorage = site.storage;
                            let siteLatency = site.latency;
                            let siteDelta   = site.delta;
                            $(__siteToParseContainer).html(siteName);
                            $(__latencyContainer).html(siteLatency);
                            i = setInterval(() => {
                                $(__latencyContainer).html(--siteLatency);
                                if (siteLatency == 0) {
                                    clearInterval(i);
                                }
                            }, 1000);
                            setTimeout(resolve, siteLatency * 1000);
                            return ajax(baseUrl + 'parser/process/parse', {
                                prodictId:   $(__productContainer).find('#product-id span').html(),
                                uniqId:      $(__sessionElementUniqIDContainer).html(),
                                site:        siteUrl,
                                productLink: $(__linkToParseContainer).html(),
                                delta:       siteDelta,
                                storage:     siteStorage,
                                session:     sessionFilePath
                            });
                        }
                    })
                    .then(parser => {
                        $(__parserStatusContainer).html(parser);
                    });

            }).then(loop.bind(null, i + 1));
        })(1);

    });
});